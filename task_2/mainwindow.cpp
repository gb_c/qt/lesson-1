#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->dsb_1->setRange(0, 10000);
    ui->dsb_2->setRange(0, 10000);
    ui->dsb_3->setRange(0, 10000);
    ui->dsb_angle->setRange(0, 10000);

    connect(ui->pb_count, SIGNAL(clicked()), this, SLOT(slotCount()));

    setWindowTitle("Side calc");
}


MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::slotCount()
{
    double side_1 = ui->dsb_1->value();
    double side_2 = ui->dsb_2->value();
    double angle = ui->dsb_angle->value();

    angle *= (ui->rb_2->isChecked())?(57.2958):(1);

    ui->dsb_3->setValue( qSqrt(qPow(side_1, 2) + qPow(side_2, 2) -
                         2 * side_1 * side_2 * qCos(angle)) );
}
