#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->sb_1->setRange(-1000, 1000);
    ui->sb_2->setRange(-1000, 1000);
    ui->sb_3->setRange(-1000, 1000);
    ui->le_solution->setReadOnly(true);

    connect(ui->pb_1, SIGNAL(clicked()), this, SLOT(slotCount()));

    setWindowTitle("Task 1");
}


MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::slotCount()
{
    QString result;

    if(ui->sb_1->value() != 0)
    {
        int d = qPow(ui->sb_2->value(), 2) - 4 * ui->sb_1->value() * ui->sb_3->value();

        qInfo() << d;

        if(d > 0)
        {
            result.append("x1 = ")
                  .append(QString::number( (-ui->sb_2->value() + qSqrt(d)) / (2 * ui->sb_1->value()), 'f' ))
                  .append(", x2 = ")
                  .append(QString::number( (-ui->sb_2->value() - qSqrt(d)) / (2 * ui->sb_1->value()), 'f' ));
        }
        else
        {
            if(d == 0)
            {
                result.append("x = ")
                      .append(QString::number(static_cast<double>(-ui->sb_2->value()) / (2 * ui->sb_1->value()), 'f'));
            }
            else
            {
                result = "Рациональных корней нет (дискриминант равен 0)";
            }
        }
    }
    else
    {
        result = "Это не квадратное уравнение a = 0";
    }

    ui->le_solution->setText(result);
}
