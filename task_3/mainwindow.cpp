#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->pb_add, &QPushButton::clicked, this, [&](){
        ui->pte_2->appendPlainText(ui->pte_1->toPlainText());
    });

    connect(ui->pb_replace, &QPushButton::clicked, this, [&](){
        ui->pte_2->clear();
        ui->pte_2->appendPlainText(ui->pte_1->toPlainText());
    });

    connect(ui->pb_addHtml, &QPushButton::clicked, this, [&](){
        ui->pte_2->clear();
        ui->pte_2->appendHtml("<font color='red'>Hello</font>");
    });
}


MainWindow::~MainWindow()
{
    delete ui;
}

